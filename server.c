#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#define BUF_SIZE 1000

int main() {

    // https://gist.github.com/hostilefork/f7cae3dc33e7416f2dd25a402857b6c6
    // https://www.tenouk.com/Module41c.html

    // https://blogs.igalia.com/dpino/2018/01/12/more-practical-snabb/
    // https://datatracker.ietf.org/doc/html/rfc6763
    // https://docs.oracle.com/cd/E19683-01/816-5042/sockets-5/index.html

    char device_name[] = "a-device"

    uint8_t buffer[BUF_SIZE];

    // set up the socket
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0) {
        perror("Socket error");
        return 1;
    }

    // load the multicast address into sockaddr_in, using known mcast IP and port for mDNS
    struct sockaddr_in mcast_addr;
    memset(mcast_addr.sin_zero, 0, sizeof(mcast_addr.sin_zero));
    if (inet_pton(AF_INET, "224.0.0.251", &(mcast_addr.sin_addr)) <= 0) {
        perror("pton failed");
        goto exit_error;
    }
    mcast_addr.sin_family = AF_INET;
    mcast_addr.sin_port = htons(5353);

    // disable receiving packets on loopback
    int multicast_opt = 0;
    if (setsockopt(sock, IPPROTO_IP, IP_MULTICAST_LOOP, &multicast_opt, sizeof(multicast_opt)) == -1) {
        perror("Set IP_MULTICAST_LOOP option failed");
        goto exit_error;
    }

    // enable reusing the same port
    int reuse_opt = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &reuse_opt, sizeof reuse_opt) == -1) {
        perror("Set SO_REUSEADDR option failed");
        goto exit_error;
    }

    // bind to receive packets
    if (bind(sock, (struct sockaddr*) &mcast_addr, sizeof mcast_addr) < 0) {
        perror("Bind failed");
        goto exit_error;
    }

    // join the multicast group to receive packets
    struct ip_mreq mreq;
    mreq.imr_interface.s_addr = INADDR_ANY;
    if (net_pton(AF_INET, "224.0.0.251", &(mreq.imr_multiaddr)) <= 0) {
        perror("pton failed");
        goto exit_error;
    }
    if (setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof mreq) == -1) {
        perror("IP Multicast join failed");
        goto exit_error;
    }

    // connect to avoid repeating the mcast IP since it's always the same
    int c = connect(sock, (struct sockaddr*) &mcast_addr, sizeof mcast_addr);
    if (c < 0) {
        perror("Connect failed");
        goto exit_error;
    }

    // send initial query to see if this device exists on the network yet 
    char msg[] = "bro that's lit";
    int len = 0;
    bytes_sent = -1;
    bytes_sent = send(sock, msg, len, 0);

    printf("sent a packet\n");

    printf("waiting for a packet...\n");

    int r = recv(sock, buffer, BUF_SIZE, 0);
    for (int i = 0; i < BUF_SIZE; i++) {
        printf("%d ", buffer[i]);
    }

    close(sock);
    return 0;

exit_error:
    close(sock);
    return 1;
}